def sum_product(my_list: list) -> int:
    x = my_list[0] + 2 * sum(my_list[1:])
    return x


my_list = [3, 3, 4, 5]
print(sum_product(my_list))

