def sum_list(start: int, end: int) -> int:
    new_list = list(range(start, end + 1))
    s = sum(new_list)
    return s


a = sum_list(1, 3)
print(a)


def sum_list(start: int, end: int) -> list:
    new_list = list(range(start, end + 1))
    return new_list


a = sum_list(1, 3)
print(a)


# Otra cosa
def sum_list(start: int, end: int = 10) -> int:
    new_list = list(range(start, end + 1))
    s = sum(new_list)
    return s


a = sum_list(1)
print(a)


def my_sum(*args):
    result = 0
    for x in args:
        result += x
    return result


my_sum(1, 2, 3)

list1 = [1, 2, 3]
list2 = [4, 5]


def sum_product(*args) -> int:
    x = args[0] + 2 * sum(args[1:])
    return x


def named(**kwargs):
    for key in kwargs():
        print('arg:', key, 'has value:', kwargs[key])

